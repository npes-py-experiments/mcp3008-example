# mcp3008 example

A small example of using the mcp3008 10-bit ADC with a raspberry Pi (3) and adafruits circuitpython library 

# Development usage

You need Python above version 3.5, pip installed and SPI enabled on the Raspberry Pi 

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/mcp3008-example.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements `pip install -r requirements.txt`
4. Run `python3 app.py` (linux) or `py app.py` (windows)

# Notes

[https://github.com/adafruit/Adafruit_CircuitPython_MCP3xxx](https://github.com/adafruit/Adafruit_CircuitPython_MCP3xxx) is returning 16 bit values to maintain consistency between libraries in circuitpython. I changed that by dividing the value with 64 to show 10 bit values for learning purposes.